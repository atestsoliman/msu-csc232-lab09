/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file TopDownFibCalculator.h
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Your Name <yname@missouristate.edu>
 *          Partner Name <pname@missouristate.edu>
 * @brief   Specification of TopDownFibCalculator.
*/

#ifndef LAB09_TOP_DOWN_FIB_CALCULATOR_H__
#define LAB09_TOP_DOWN_FIB_CALCULATOR_H__

#include "FibCalculator.h"

/**
 * An implementation of the <code>FibCalculator</code> that uses top-down dynamic programming approaches where
 * appropriate.
 */
class TopDownFibCalculator : public FibCalculator {
public:
    /**
     * Calculates the nth Fibonacci number using a bottom-up, dynamic programming approach.
     *
     * @pre n >= 0
     * @param n an index into the Fibonaaci sequence beginning at 0.
     * @return The nth number in the Fibonacci sequence is returned.
     */
    int nthFibonacciNumber(int n) const override;
    virtual ~TopDownFibCalculator() {}
};

#endif //LAB09_TOP_DOWN_FIB_CALCULATOR_H__
